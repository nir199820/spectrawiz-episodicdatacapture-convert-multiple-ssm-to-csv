This project will allow you to convert the multiple .ssm file that are created by the program SpectraWiz to a united
CSV file containing the wavelength as the first row and for each wavelength a column of the measured signals.

For this code to work make sure that you insert the URL of your folder containing the ssm files to the parameter:
"measurements_folder"

The united file will be saved in the same directory under the name "united_data.csv"