import os
import pandas as pd
from pathlib import Path
from collections import defaultdict
import csv

#----------insert files directory here----------:
measurements_folder = r'E:\מעבדה בכימיה פיזיקלית 2\מעבדה בכימיה פיזיקלית 2 - ניסוי 9 - קינטיקה כימית\part 3 - data\test data'


def change_ssm_csv_extension(directory):
    for file_name in os.listdir(directory):
        if file_name.endswith("Ep.ssm") || file_name.endswith("Ep.abs"):
            file_path = Path(measurements_folder + '\\' + file_name)
            file_path.rename(file_path.with_suffix('.csv'))


def create_wavelength_signal_dictionary(directory):
    # Initialize an empty dictionary to store the mappings
    wavelength_signal_dict = defaultdict(list)

    # Loop through the files in the directory
    for file_name in os.listdir(directory):
        if file_name.endswith("Ep.csv"):
            file_path = Path(measurements_folder + '\\' + file_name)

            try:
                # Try reading the file with 'utf-8' encoding
                df = pd.read_csv(file_path, encoding='utf-8')
            except UnicodeDecodeError:
                # If utf-8 encoding doesn't work, try 'latin1'
                df = pd.read_csv(file_path, encoding='latin1')

            # Assuming the column containing image paths is named 'Image_Path'
            for index, row in df.iterrows():
                wavelength, signal = row.values[0].split()

                # Create a mapping between image path and value
                wavelength_signal_dict[wavelength].append(signal)
                # Now, image_value_dict contains the mappings between image paths and values
    return wavelength_signal_dict


change_ssm_csv_extension(measurements_folder)
wavelength_signal_dict = create_wavelength_signal_dictionary(measurements_folder)

with open(measurements_folder + '\\' + "united_data.csv", "w", newline="") as csvfile:
    csv_writer = csv.writer(csvfile)

    # Write the header row (keys)
    csv_writer.writerow(wavelength_signal_dict.keys())

    # Write the data rows (values)
    csv_writer.writerows(zip(*wavelength_signal_dict.values()))

print("Data extracted successfully to the file united_data.csv, Have fun with your data analysis!")
